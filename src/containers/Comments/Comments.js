import React from 'react';
import { Box } from '@material-ui/core';
import { Comment } from '../../components/Comment/Comment';

export const Comments = ({ commentIds }) => {
  return (
    <>
      {commentIds.map((id) => (
        <Box ml={3} py={2} key={id}>
          <Comment commentId={id} />
        </Box>
      ))}
    </>
  );
};
