import React, { useEffect, useState } from 'react';
import { Box, Button, Grid } from '@material-ui/core';
import { getNewStoryIds } from '../../api/hackernewsApi';
import { Story } from '../../components/Story/Story';
import { MAX_ITEMS_COUNT, PAGE_STEP_COUNT } from '../../constants/constants';

export const Stories = () => {
  const [storyIds, setStoryIds] = useState([]);
  const [favoritesList, setFavoritesList] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
  const [pageItemsFrom, setPageItemsFrom] = useState(0);
  const [pageItemsTo, setPageItemsTo] = useState(PAGE_STEP_COUNT);

  useEffect(() => {
    getNewStoryIds().then((res) => setStoryIds(res));
    const updateInterval = setInterval(() => getNewStoryIds().then((res) => setStoryIds(res)), 20000);
    return () => clearInterval(updateInterval);
  }, []);

  return (
    <>
      <Box py={2}>
        <Grid container spacing={1} direction="column">
          {favoritesList.map((id) => (
            <Grid key={id} item>
              <Story
                favoritesList={favoritesList}
                setFavoritesList={setFavoritesList}
                isInFavorites={true}
                storyId={id}
              />
            </Grid>
          ))}
        </Grid>
        <Grid container spacing={1} direction="column">
          {storyIds.slice(pageItemsFrom, pageItemsTo).map((id) => {
            return !favoritesList.includes(id) ? (
              <Grid key={id} item>
                <Story
                  key={id}
                  favoritesList={favoritesList}
                  setFavoritesList={setFavoritesList}
                  isInFavorites={false}
                  storyId={id}
                />
              </Grid>
            ) : null;
          })}
        </Grid>
      </Box>
      <Box>
        {pageItemsFrom >= PAGE_STEP_COUNT && (
          <Button
            variant={'contained'}
            onClick={() => {
              setPageItemsFrom(pageItemsFrom - PAGE_STEP_COUNT);
              setPageItemsTo(pageItemsTo - PAGE_STEP_COUNT);
            }}
          >
            PREVIOUS
          </Button>
        )}
        {pageItemsTo < MAX_ITEMS_COUNT && (
          <Button
            variant={'contained'}
            onClick={() => {
              setPageItemsFrom(pageItemsFrom + PAGE_STEP_COUNT);
              setPageItemsTo(pageItemsTo + PAGE_STEP_COUNT);
            }}
          >
            NEXT
          </Button>
        )}
      </Box>
    </>
  );
};
