import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Grid, Box, Container } from '@material-ui/core';
import { Stories } from './containers/Stories/Stories';

export const App = () => {
  return (
    <>
      <CssBaseline />
      <Box p={3}>
        <Container>
          <Grid
            container
            spacing={1}
            direction="column"
            justify="center"
            alignItems="center"
            style={{ backgroundColor: '#fff2eb' }}
          >
            <h1>Hacker News Assignment</h1>
            <Stories />
          </Grid>
        </Container>
      </Box>
    </>
  );
};
