export const NEW_STORY_URL = '/newstories.json';
export const SINGLE_ITEM_URL = '/item';
export const MAX_ITEMS_COUNT = 500;
export const PAGE_STEP_COUNT = 20;
