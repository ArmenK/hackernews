import React, { useEffect, useState } from 'react';
import { Box, Paper } from '@material-ui/core';
import moment from 'moment';
import { getSingleItem } from '../../api/hackernewsApi';
import styles from './Story.module.css';
import { Comments } from '../../containers/Comments/Comments';

export const Story = ({ storyId, isInFavorites, favoritesList, setFavoritesList }) => {
  const [story, setStory] = useState({});
  const [showComments, setShowComments] = useState(false);

  useEffect(() => {
    getSingleItem(storyId).then((res) => res && res.url && setStory(res));
  }, [storyId]);

  const addFavorite = (storyId) => {
    setFavoritesList([storyId, ...favoritesList]);
    localStorage.setItem('favorites', JSON.stringify([storyId, ...favoritesList]));
  };

  const removeFavorite = (storyId) => {
    const updatedFavoritesList = favoritesList.filter((item) => item !== storyId);
    setFavoritesList(updatedFavoritesList);
    localStorage.setItem('favorites', JSON.stringify(updatedFavoritesList));
  };

  const favoriteButton = isInFavorites ? (
    <span className={styles.action_item} onClick={() => removeFavorite(story.id)}>
      unfavorite
    </span>
  ) : (
    <span className={styles.action_item} onClick={() => addFavorite(story.id)}>
      favorite
    </span>
  );

  const comments = story.kids ? (
    <span className={styles.action_item} onClick={() => setShowComments(!showComments)}>
      {story.kids.length} comments |{' '}
    </span>
  ) : (
    <span>0 comments | </span>
  );

  const commentsBlock = showComments ? (
    <div>
      <Comments commentIds={story.kids} />
    </div>
  ) : null;

  const formattedTime = moment.unix(story.time).fromNow();

  return story && story.url ? (
    <Paper variant={'outlined'}>
      <Box p={1} maxWidth={520} className={isInFavorites ? styles.favorite_box : null}>
        <div>
          <a className={styles.title_link} href={story.url} rel="noreferrer" target={'_blank'}>
            {story.title}
          </a>
        </div>
        <span>{story.score} points | </span>
        <span>by: {story.by} | </span>
        {comments}
        <span>{formattedTime} | </span>
        {favoriteButton}
        {commentsBlock}
      </Box>
    </Paper>
  ) : null;
};
