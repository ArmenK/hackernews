import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { getSingleItem } from '../../api/hackernewsApi';
import { Comments } from '../../containers/Comments/Comments';

export const Comment = ({ commentId }) => {
  const [comment, setComment] = useState({});

  useEffect(() => {
    getSingleItem(commentId).then((res) => res && setComment(res));
  }, [commentId]);

  const commentsBlock = comment.kids ? (
    <div>
      <Comments commentIds={comment.kids} />
    </div>
  ) : null;

  const formattedTime = moment.unix(comment.time).fromNow();

  return (
    <>
      <span>{comment.by} | </span>
      <span>{formattedTime}</span>
      <p dangerouslySetInnerHTML={{ __html: comment.text }}></p>
      {commentsBlock}
    </>
  );
};
