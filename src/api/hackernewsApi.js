import { axiosInstance } from './axios-instance';
import { NEW_STORY_URL, SINGLE_ITEM_URL } from '../constants/constants';

export const getNewStoryIds = async () => {
  const result = await axiosInstance
    .get(NEW_STORY_URL)
    .then((res) => res.data && res.data)
    .catch((err) => console.error(err));

  return result;
};

export const getSingleItem = async (itemId) => {
  const result = await axiosInstance
    .get(`${SINGLE_ITEM_URL}/${itemId}.json`)
    .then((res) => res.data && res.data)
    .catch((err) => console.error(err));

  return result;
};
